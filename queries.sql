USE reservation;

--1

SELECT Passenger.namePassenger, Passenger.surnamePassenger
FROM Passenger, Ticket, Flight, Airport
WHERE Passenger.countryPassenger = 'United Kingdom (Great Britain)'
AND Airport.idAirport = Flight.toAirport
AND Flight.idFlight = Ticket.idFlight
AND Ticket.idPassenger = Passenger.idPassenger
;

--1
--все граждани Британии и их аэропорты прилета 

SELECT CONCAT (p.namePassenger, ' ', p.surnamePassenger) `Пассажиры`, nameAirport 
FROM Passenger p INNER JOIN ( 
	SELECT idPassenger, nameAirport 
    FROM Ticket INNER JOIN (
		SELECT idFlight, nameAirport 
        FROM Flight INNER JOIN ( 
			SELECT idAirport, nameAirport 
            FROM Airport 
		) a 
		ON a.idAirport = Flight.toAirport 
	) f 
	ON f.idFlight = Ticket.idTicket 
) t 
ON t.idPassenger = p.idPassenger 
WHERE p.countryPassenger = 'United Kingdom (Great Britain)'
;		

--2

SELECT namePassenger, surnamePassenger
FROM Passenger, Flight, Ticket
WHERE Flight.idFlight > 53 
AND Flight.aircraftFlight = 380 
AND Passenger.idPassenger = Ticket.idPassenger
AND Flight.idFlight = Ticket.idFlight
;

--2
--все пассажиры А380 рейсов ид>53

SELECT p.namePassenger `Name`, p.surnamePassenger `Surname`
FROM Passenger p INNER JOIN (
	SELECT idPassenger
    FROM Ticket INNER JOIN (
		SELECT idFlight
        FROM Flight
        WHERE aircraftFlight = 380
        AND idFlight > 53
        ) f
	ON Ticket.idFlight = f.idFlight
    ) t
ON p.idPassenger = t.idPassenger
;

--3

SELECT Airport.nameAirport
FROM Reservation, Flight, Ticket, Airport
WHERE Reservation.paymentStatus = 1 
AND Reservation.idReservation = Ticket.idReservation
AND Ticket.idFlight = Flight.idFlight
AND Flight.fromAirport = Airport.idAirport
;

--3
--все аэропорты вылета, пассажиры которых оплатили билет

SELECT a.nameAirport `Airport`
FROM Airport a INNER JOIN (
	SELECT Flight.fromAirport
    FROM Flight INNER JOIN (
		SELECT Ticket.idFlight
        FROM Ticket INNER JOIN (
			SELECT idReservation
            FROM Reservation
            WHERE paymentStatus = 1
            ) r
		ON r.idReservation = Ticket.idReservation
		) t
	ON t.idFlight = Flight.idFlight
    ) f
ON a.idAirport = f.fromAirport
;
           
--4
--все агенты, которые обслуживали пассажиров летящих в Лондон

SELECT CONCAT(ag.Name, ' ', ag.Surname) `Агент`
FROM Agent ag INNER JOIN (
	SELECT idAgent 
	FROM Ticket INNER JOIN (
		SELECT idFlight
		FROM Flight INNER JOIN (
			SELECT idAirport
			FROM Airport
			WHERE cityAirport = 'London'
			) a
			ON a.idAirport = Flight.fromAirport
		) f
		ON f.idFlight = Ticket.idFlight
	) t
ON t.idAgent = ag.idAgent
WHERE ag.selfServiceYN = 0
;

--5
--аэропорты вылета, билеты на рейсы в которые были оплачены ранее ноября 2015

SELECT a.cityAirport
FROM (
	SELECT idReservation
	FROM Reservation
	WHERE YEAR(whenDidPaidReservation) = 2015 AND MONTH(whenDidPaidReservation) < 11
 ) AS r 
JOIN Ticket AS t ON t.idReservation = r.idReservation
JOIN Flight AS f ON f.idFlight = t.idFlight
JOIN Airport AS a ON f.toAirport = a.idAirport
;

--6 
--города, куда билеты стоят < 60
SELECT a.cityAirport
FROM (
	SELECT idTicket, idFlight
	FROM Ticket
	WHERE price < 60
) AS t
JOIN Flight AS f ON t.idFlight = f.idFlight
JOIN Airport AS a ON f.toAirport = a.idAirport
;

--7
--имена пассажиров, которые оплатили в тот же месяц, что и зарезервировали (я хотел день, но таких в БД нет)
SELECT CONCAT(p.namePassenger, ' ', p.surnamePassenger) `Пассажиры`
FROM (
	SELECT namePassenger, surnamePassenger, idPassenger
	FROM Passenger
) p
JOIN Ticket AS t ON p.idPassenger = t.idPassenger
JOIN Reservation AS r ON t.idReservation = r.idReservation AND MONTH(t.reservedOnTicket) = MONTH(r.whenDidPaidReservation) AND YEAR(t.reservedOnTicket) = YEAR(r.whenDidPaidReservation)
;

--8 
--города, в которые можно летать
SELECT cityAirport
FROM Airport
;

--9
--города, в которых есть более 1 ВПП
SELECT cityAirport
FROM Airport
WHERE numRunwaysAirport > 1
;

--10
--города, аэропорты которых могут принимать борты в плохую погоду
SELECT cityAirport
FROM Airport
WHERE ilsORnavLevelAirport > 1
;

--11
--фамилии пассажиров, которые забронировали билет летом
SELECT p.surnamePassenger
FROM (
	SELECT idPassenger
	FROM Ticket
	WHERE (MONTH(reservedOnTicket) BETWEEN 5 AND 9)
) t
JOIN Passenger AS p ON t.idPassenger = p.idPassenger
;

--12
--имейлы пассажиров из UK
SELECT p.emailPassenger
FROM Passenger AS p
WHERE p.countryPassenger = 'United Kingdom (Great Britain)'
;

--13 
--фамилии пассажиров и агентов из одной страны
SELECT p.surnamePassenger, ag.Surname `surnameAgent`, p.countryPassenger
FROM (
	SELECT countryPassenger, surnamePassenger
	FROM Passenger
) p
JOIN Agent AS ag ON p.countryPassenger = ag.Country
;

--14
--длина второго имени пассажиров
SELECT p.secondNamePassenger, LENGTH(p.secondNamePassenger) AS Length
FROM Passenger AS p
;

--15
--банки пассажиров, которые оплатили билет
SELECT DISTINCT r.nameOfBankReservation
FROM Reservation AS r
WHERE paymentStatus = 1
;

--16
--аэропорты куда летает А380 и B747
SELECT a.cityAirport
FROM (
	SELECT cityAirport, idAirport
	FROM Airport
) a 
JOIN Flight AS f ON f.toAirport = a.idAirport AND f.aircraftFlight = 380 OR f.aircraftFlight = 747
;

--17 
--однофамильцы в таблице пассажиров
SELECT surnamePassenger 
FROM Passenger
GROUP BY surnamePassenger
HAVING COUNT(surnamePassenger) > 1
;

--18
--рейсы, которые уже улетели
SELECT idFlight
FROM Flight
WHERE dateFlight < NOW()
;

--19
--информация о карте оплативших билет пассажиров
SELECT r.cardInfoReservation
FROM Reservation AS r
WHERE paymentStatus = 1
;

--20
--сколько часов суммарно пролетят пассажиры после выполнения всех находящихся в базе рейсов
SELECT SUM(durationHoursFlight) AS Hours 
FROM Flight
;

--21
--города где есть аэропорты с 2+ терминалов
SELECT cityAirport
FROM Airport
WHERE numOfTerminalsAirport > 1
;

--22
--городa с максимальным числом терминалов
SELECT a.cityAirport, numOfTerminalsAirport
FROM Airport AS a 
JOIN (
	SELECT MAX(numOfTerminalsAirport) AS maxterm
	FROM Airport ma 
	) m 
ON m.maxterm = a.numOfTerminalsAirport
;

--23
--городa с максимальным числом ВПП
SELECT a.cityAirport, numRunwaysAirport
FROM Airport AS a 
JOIN (
	SELECT MAX(numRunwaysAirport) AS maxterm
	FROM Airport ma 
	) m 
ON m.maxterm = a.numRunwaysAirport
;

--24
--города с максимальным уровнем ILS
SELECT a.cityAirport, ilsORnavLevelAirport
FROM Airport AS a 
JOIN (
	SELECT MAX(ilsORnavLevelAirport) AS maxterm
	FROM Airport ma 
	) m 
ON m.maxterm = a.ilsORnavLevelAirport
;

--25
--рейсы с максимальной длительностью
SELECT f.idFlight, durationHoursFlight
FROM Flight AS f 
JOIN (
	SELECT MAX(durationHoursFlight) AS maxterm
	FROM Flight mf 
	) m 
ON m.maxterm = f.durationHoursFlight
;

--26
--самые небезопасные аэропорты в плане погодных условий
SELECT a.cityAirport, ilsORnavLevelAirport
FROM Airport AS a 
JOIN (
	SELECT MIN(ilsORnavLevelAirport) AS maxterm
	FROM Airport ma 
	) m 
ON m.maxterm = a.ilsORnavLevelAirport
;

--27
--одно-секанд-нейм в таблице пассажиров
SELECT secondNamePassenger 
FROM Passenger
GROUP BY secondNamePassenger
HAVING COUNT(secondNamePassenger) > 1
;

--28
--средняя цена за билет
SELECT AVG(price) `Средняя цена билета`
FROM Ticket
;

--29
--среднее количество ВПП в аэропортах, с которыми работаем
SELECT AVG(numRunwaysAirport) `Среднее количество ВПП по миру`
FROM Airport
;

--30
--пассажиры из городов, в которых есть обслуживаемые аэропорты
SELECT CONCAT(p.namePassenger, ' ', p.surnamePassenger) `Пассажиры`, a.nameAirport `Город`
FROM (
	SELECT idPassenger, namePassenger, surnamePassenger, cityPassenger
	FROM Passenger
) p
JOIN Ticket AS t ON p.idPassenger = t.idPassenger
JOIN Flight AS f ON t.idFlight = f.idFlight
JOIN Airport AS a ON f.fromAirport = a.idAirport
;



--распродажа
--триггер, который при вставке новой строки в таблицу билетов уменьшает цену у всех остальных на 1

DELIMITER //
CREATE TRIGGER saleChecker 
BEFORE INSERT ON Ticket FOR EACH ROW 
BEGIN
	UPDATE Ticket
	SET price = price - 1;
END
;
// DELIMITER ;










